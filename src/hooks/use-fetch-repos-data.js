import { useEffect, useState } from 'react';
import axios from 'axios';
import { urls } from '../constants/backend-urls';

export const useFetchReposData = () => {
    const [data, setData] = useState([]);
    const [url, setUrl] = useState(urls.github.facebook.repos);
    const [isLoading, setIsLoading] = useState(false);
    const [isError, setIsError] = useState(false);

    useEffect(() => {
        const fetchData = async () => {
            setIsError(false);
            setIsLoading(true);
            try {
                const result = await axios(url);
                setData(result.data);
            } catch (error) {
                setIsError(true);
            }
            setIsLoading(false);
        };
        fetchData();
    }, [url]);
    return [{ data, isLoading, isError }, setUrl];
};
