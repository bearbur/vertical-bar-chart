import React from 'react';
import styled from 'styled-components';

const ErrorOnLoadingDataWarningWrapper = styled.div``;

const ErrorOnLoadingDataWarning = () => {
    return (
        <ErrorOnLoadingDataWarningWrapper>
            <span>Error on loading data. Please, reload the page.</span>
        </ErrorOnLoadingDataWarningWrapper>
    );
};

export default ErrorOnLoadingDataWarning;
