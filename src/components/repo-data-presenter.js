import React, { useEffect, useRef } from 'react';
import { array } from 'prop-types';
import styled from 'styled-components';
import * as d3 from 'd3';
import { throttle } from 'lodash';

const Canvas = styled.canvas`
    padding: 10px;
`;

const RepoDataPresenterWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: flex-start;
    padding-top: 10px;
    margin: 0;
`;

const RepoDataPresenterTitle = styled.span`
    width: 328px;
    height: 22px;
    color: #333333;
    font-size: 18px;
    font-weight: 400;
    line-height: 22px;
`;

const RepoDataPresenterDetails = styled.span`
    z-index: 4;
    width: 1348px;
    height: 16px;
    color: #3b3b3b;
    font-size: 12px;
    font-weight: 400;
    line-height: 16px;
`;

const RepoDataPresenter = ({ repoData }) => {
    const canvasWidth = 1250,
        canvasHeight = 650,
        deltaAxisLabelPosition = {
            x0: 20,
            y0: 20,
            yText: 200,
            xText: 30,
        },
        horizontalLineCount = 9,
        fontColor = '#000',
        lineWidth = 0.5,
        subLineWidth = 0.25,
        labels = {
            font: '11px Sans-Serif',
        },
        barColor = '#68c98e',
        delay = 0,
        speed = 1;

    const canvasElement = useRef(null);

    const widthOfBar = Math.round((0.75 * canvasWidth) / repoData.length);

    const draw0XAxis = (context) => {
        context.beginPath();
        context.moveTo(0, canvasHeight - deltaAxisLabelPosition.yText);
        context.lineTo(canvasWidth, canvasHeight - deltaAxisLabelPosition.yText);
        context.lineWidth = lineWidth;
        context.stroke();
    };

    const draw0YAxis = (context) => {
        context.beginPath();
        context.moveTo(deltaAxisLabelPosition.xText, deltaAxisLabelPosition.y0);
        context.lineTo(deltaAxisLabelPosition.xText, canvasHeight - 9.5 * deltaAxisLabelPosition.y0);
        context.lineWidth = lineWidth;
        context.stroke();
    };

    const draw0XSubAxis = (context, startLineYPosition) => {
        context.beginPath();
        context.moveTo(0, startLineYPosition);
        context.lineTo(canvasWidth, startLineYPosition);
        context.lineWidth = subLineWidth;
        context.stroke();
    };

    const updateCanvas = (firstRender) => {
        const context = canvasElement.current.getContext('2d');

        // Make scale x
        const dataScale0X = d3.range(repoData.length).map((d) => d);

        // Create  0x scale
        const x = d3
            .scaleLinear()
            .range([deltaAxisLabelPosition.x0, canvasWidth - deltaAxisLabelPosition.x0])
            .domain(d3.extent(dataScale0X));

        // Make scale y
        const dataScale0Y = d3.range(d3.max(repoData.map((d) => d.open_issues))).map((d) => d);
        const maxOpenIssues = d3.max(repoData.map((d) => d.open_issues));

        // Create 0y scale
        const y = d3
            .scaleLinear()
            .range([deltaAxisLabelPosition.y0, canvasHeight - deltaAxisLabelPosition.y0])
            .domain(d3.extent(dataScale0Y));

        // Draw main axis
        draw0XAxis(context);
        draw0YAxis(context);

        context.font = labels.font;

        // Draw labels for 0y scale and horizontal sub axis

        for (let lineNumber = 0; lineNumber < horizontalLineCount; lineNumber++) {
            context.save();
            context.translate(
                0,
                (canvasHeight - 9 * deltaAxisLabelPosition.y0) * (1 - lineNumber / horizontalLineCount)
            );

            context.textAlign = 'right';
            context.fillStyle = fontColor;

            if (lineNumber === 0) {
                context.fillText(0, 25, 0);
            } else {
                context.fillText(Math.round(y((lineNumber * maxOpenIssues) / horizontalLineCount)), 25, 0);
                draw0XSubAxis(context, 0);
            }

            context.restore();
        }

        const barHeightCoefficient = (canvasHeight - 11 * deltaAxisLabelPosition.y0) / maxOpenIssues;

        // Draw vertical bars with labels - repositories names

        for (let repo of repoData) {
            //Add text label 0x scale from repoData and rotate it on PI/4 degree

            context.save();
            context.translate(
                deltaAxisLabelPosition.x0 + x(repoData.indexOf(repo)),
                canvasHeight - 9 * deltaAxisLabelPosition.y0
            );
            context.rotate(-Math.PI / 4);
            context.textAlign = 'right';
            context.fillStyle = fontColor;
            context.fillText(repo.name.substring(0, 15), 20, 0);
            context.restore();

            //Draw bar with delay

            context.fillStyle = barColor;

            const heightOfBar = Math.round(barHeightCoefficient * repo.open_issues);
            const yStartPosition = canvasHeight - 10 * deltaAxisLabelPosition.y0;

            if (firstRender) {
                for (let l = 0; l < heightOfBar; ++l) {
                    setTimeout(
                        context.fillRect.bind(
                            context,
                            x(repoData.indexOf(repo)),
                            yStartPosition - heightOfBar,
                            widthOfBar,
                            l
                        ),
                        (l > 0 ? delay + x(repoData.indexOf(repo)) * speed : 0) + delay + l * speed
                    );
                }
            } else {
                context.fillRect(
                    x(repoData.indexOf(repo)),
                    canvasHeight - 10 * deltaAxisLabelPosition.y0 - barHeightCoefficient * repo.open_issues,
                    widthOfBar,
                    barHeightCoefficient * repo.open_issues
                );
            }

            context.restore();
        }
    };

    useEffect(() => {
        updateCanvas(true);
    });

    const handleMouseMove = throttle((e) => {
        e.persist();

        if (!e || !e.nativeEvent) {
            return;
        }

        const context = canvasElement.current.getContext('2d');
        const nativeEvent = e.nativeEvent;
        const paddingXValue = 60;
        const paddingYValue = 70;
        const xCoordinate = nativeEvent.clientX - paddingXValue;
        const yCoordinate = nativeEvent.clientY - paddingYValue;

        // Make scale x
        const dataScale0X = d3.range(repoData.length).map((d) => d);

        // Take 0x scale
        const x = d3
            .scaleLinear()
            .range([deltaAxisLabelPosition.x0, canvasWidth - deltaAxisLabelPosition.x0])
            .domain(d3.extent(dataScale0X));

        for (let repo of repoData) {
            const startBarX = x(repoData.indexOf(repo));
            const endBarX = startBarX + widthOfBar;

            if (xCoordinate >= startBarX && xCoordinate <= endBarX) {
                // When coordinates of mouse on vertical bar - show tooltip with open issues

                context.fillStyle = fontColor;
                context.fillText(repo.open_issues, startBarX, 10);
                break;
            } else {
                // Rerender and clear tooltip

                context.clearRect(0, 0, canvasWidth, canvasHeight);
                updateCanvas();
            }
        }
    }, 250);

    return (
        <RepoDataPresenterWrapper>
            <RepoDataPresenterTitle>{repoData.length} repos.</RepoDataPresenterTitle>
            <RepoDataPresenterDetails>Data was fetched from github.</RepoDataPresenterDetails>
            <Canvas ref={canvasElement} width={canvasWidth} height={canvasHeight} onMouseMove={handleMouseMove} />
        </RepoDataPresenterWrapper>
    );
};

RepoDataPresenter.propTypes = {
    repoData: array.isRequired,
};

export default RepoDataPresenter;
