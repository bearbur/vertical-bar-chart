import React from 'react';
import styled from 'styled-components';
import { string } from 'prop-types';

const PageTitleWrapper = styled.div`
    background: rgba(104, 201, 142, 1);
    width: 100%;
    display: flex;
    flex-direction: row;
    justify-content: center;
`;

const PageTitleText = styled.span`
    color: rgba(255, 255, 255, 0.9);
    font-size: 1.2em;
`;

const PageTitle = ({ pageTitle }) => {
    return (
        <PageTitleWrapper>
            <PageTitleText>{pageTitle}</PageTitleText>
        </PageTitleWrapper>
    );
};

PageTitle.propTypes = {
    pageTitle: string.isRequired,
};

export default PageTitle;
