import React from 'react';
import styled from 'styled-components';
import { useFetchReposData } from '../hooks/use-fetch-repos-data';
import DataIsLoading from './data-is-loading';
import ErrorOnLoadingDataWarning from './error-on-loading-data-warning';
import RepoDataPresenter from './repo-data-presenter';

const RepoDataBlockWrapper = styled.div`
    width: 1351px;
    height: 768px;
    background-color: rgba(255, 255, 255, 1);
`;

const RepoDataBlock = () => {
    const [{ data, isLoading, isError }, setUrl] = useFetchReposData();

    return (
        <RepoDataBlockWrapper>
            {isLoading && <DataIsLoading />}
            {isError && <ErrorOnLoadingDataWarning />}
            {!isLoading && !isError && <RepoDataPresenter repoData={data} />}
        </RepoDataBlockWrapper>
    );
};

export default RepoDataBlock;
