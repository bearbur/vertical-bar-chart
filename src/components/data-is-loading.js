import React from 'react';
import styled from 'styled-components';

const DataIsLoadingWrapper = styled.div``;

const DataIsLoading = () => {
    return (
        <DataIsLoadingWrapper>
            <span>Processing...</span>
        </DataIsLoadingWrapper>
    );
};

export default DataIsLoading;
