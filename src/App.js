import React from 'react';
import PageTitle from './components/page-title';
import styled from 'styled-components';
import RepoDataBlock from './components/repo-data-block';

const AppWrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    height: 100vh;
    width: 100vw;
    background: rgba(255, 255, 255, 1);
    padding: 0;
    font-family: sans-serif;
    letter-spacing: 0.1em;
`;

const App = () => {
    return (
        <AppWrapper>
            <PageTitle pageTitle={'Facebook repos'} />
            <RepoDataBlock />
        </AppWrapper>
    );
};

export default App;
