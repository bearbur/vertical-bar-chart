# vertical bar chart



1. Создать чистый проект на React (приложение должно запускаться через команду npm start).

2. В App.js отрендерить страницу с заголовком "Facebook repos"

3. С помощью api github дернуть список репозиториев Facebook (описание api https://developer.github.com/v3/)

4. С помощью библиотеки визуализации d3 нарисовать на странице vertical bar chart с зависимостью количества open_issues от имени репозитория. Использовать canvas(обязательно)

4.1 Добавить анимацию отрисовки столбцов после получения данных (опционально)

4.2 Добавить tooltip при наведении на столбец  (опционально)
